#!/bin/bash

scp .env ariel@rpi.arielvinas.com.ar:~/
scp docker-compose.yaml ariel@rpi.arielvinas.com.ar:~/
scp nginx/nginx.conf ariel@rpi.arielvinas.com.ar:~/nginx
scp nginx/default.conf ariel@rpi.arielvinas.com.ar:~/nginx
scp nginx/Dockerfile ariel@rpi.arielvinas.com.ar:~/nginx
ssh ariel@rpi.arielvinas.com.ar ' /usr/bin/docker-compose up -d --build && exit'